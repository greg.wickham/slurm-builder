
#https://download.schedmd.com/slurm/slurm-18.08.1.tar.bz2

include Makefile.variables

#OS_ID := $(shell lsb_release --id --short )
#OS_RELEASE := $(shell lsb_release --release --short )

OS_ID := $(shell facter os.name )
OS_RELEASE := $(shell facter os.release.full )

# detecte OFED version . . needs more testing
OS_OFED := $(shell test -d /usr/share/doc/libibverbs-*mlnx* ; \
	if [[ $$? -eq "0" ]]; then echo "MLNX" ; else echo "STD"; fi )

#${OS_OFED}

GIT_HEAD := $(shell git rev-parse HEAD )

INSTALL_HASH 	:= ${SLURM_RELEASE}${SLURM_SUB_RELEASE}/${GIT_HEAD}/${OS_ID}-${OS_RELEASE}
TOUCH_HASH		:= $(shell echo ${INSTALL_HASH} | sed -e 's/\//-/g')

BUILD_DIR		?=	build/${SLURM_RELEASE}/${OS_ID}-${OS_RELEASE}

SLURM_BUILD 	:= ${BUILD_DIR}/slurm-${SLURM_RELEASE}

# can be overridden with -DINSTALL_BASE=...
INSTALL_BASE	?=	/opt/slurm/install

# can be overridden with -DINSTALL_DIR=...
INSTALL_DIR 	?= ${INSTALL_BASE}/slurm-${INSTALL_HASH}

INSTALL_LOCK	:=	$(INSTALL_DIR)/.lock

ifneq ("$(wildcard $(INSTALL_LOCK))","")
    LOCK_ACTIVE = 1
else
    LOCK_ACTIVE = 0
endif

# can be overridden with -DSTAGING=...
STAGING			?=	staging

STAGING_DIR		:= $(shell pwd)/${STAGING}/

# Build targets (added to by each module)
#
BUILD_TARGET	:=
INSTALL_TARGET	:=

.PHONY:	all

all:
	@( \
		if [[ -f $(INSTALL_LOCK) ]]; then \
			echo "Cannot deploy: $(INSTALL_LOCK) exists" ; \
			echo "Lock file must be removed to re-deploy" ; \
			echo "Use 'make build' to test building, or 'make 'install' to build a staging tree" ; \
			exit 1 ; \
		fi \
	)
	$(MAKE) deploy

.PHONY: clean

clean:	clean-build clean-staging

.PHONY: clean-build

clean-build:
	rm -rf $(BUILD_DIR)/*

.PHONY: clean-staging

clean-staging:
	rm -rf staging

$(STAGING_DIR):
	mkdir -pv $(STAGING_DIR)

xx:
	@echo "copying from ${STAGING_DIR}${INSTALL_DIR} to ${INSTALL_DIR}"
	echo "@tar -C ${STAGING_DIR}/opt/slurm/install -cf - . | tar -C /opt/slurm/install -xvf -"
	exit 1
	@tar -C ${STAGING_DIR}/opt/slurm/install -cf - . | tar -C /opt/slurm/install -xvf -
	@echo "installed ${INSTALL_DIR}"
	touch ${TAG_DEPLOY}

#${STAGING_DIR}:
#	@mkdir -v ${STAGING_DIR}

.PHONY: sanity-check
sanity-check:
	@git diff-index --quiet HEAD --
	@if [ -d ${INSTALL_DIR} ]; then echo "${INSTALL_DIR} already exists" ; exit 1 ; fi

# And include the partial build modules

# Do not permit installs to be performed over the top of an existing install
# Due to slurm making extensive use of shared libraries running installations
# will need to be restarted (including slurmstepd which isn't possible)

.PHONY:	build-dirs

build-dirs:
	@mkdir -vp srcs $(BUILD_DIR) $(STAGING_DIR)

.PHONY:	warning

warning:
	@( \
		if [[ -f $(INSTALL_LOCK) ]]; then \
			echo "Cannot deploy: $(INSTALL_LOCK) exists" ; \
			echo "Lock file must be removed to re-deploy" ; \
			echo "Use 'make build' to test building, or 'make 'install' to build a staging tree" ; \
			exit 1 ; \
		fi \
	)

include Makefile.pmix
include Makefile.slurm
include Makefile.spank
include Makefile.drmma
include Makefile.extras
include Makefile.deploy

.PHONY:	build

build : ${BUILD_TARGET}

.PHONY:	install

install : build ${INSTALL_TARGET}

# END OF FILE
