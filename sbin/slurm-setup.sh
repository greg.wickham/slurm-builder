#!/bin/bash

DIR="/opt/slurm/install/${1}"

if [[ -z "${1}" ]]; then
    echo "usage: $( basename $0 ) <slurm-install-version>"
    exit 1
elif [[ ! -d "${DIR}" ]]; then
    echo "slurm version ${1} is not present in /opt/slurm/install"
    exit 1
fi

echo "Using ${DIR}"

# copy the bash profile template

