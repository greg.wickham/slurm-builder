#!/bin/bash

# this script will install the 'system' files into the correct 
# place on a node.

SLURM_BASE="/opt/slurm/install/slurm-@SLURM_INSTALL@"

if [[ ! -d "${SLURM_BASE}" ]]; then
    echo "directory ${SLURM_BASE} doesn't exist"
    echo "usage: $( basename $0 ) <cluster-name>"
    exit 1
fi

echo "* installing logrotate configuration"
for file in ${SLURM_BASE}/etc/logrotate.d/*; do
    install -v -m 644 -o root -g root ${file} /etc/logrotate.d/
done

echo "* installing bash profile"
for file in ${SLURM_BASE}/etc/profile.d/*; do
    install -v -m 755 -o root -g root ${file} /etc/profile.d/
done

echo "* installing ld.so.conf library paths"
for file in ${SLURM_BASE}/etc/ld.so.conf.d/*; do
    install -v -m 755 -o root -g root ${file} /etc/ld.so.conf.d/
done

echo "* installing systemd unit files"
for file in ${SLURM_BASE}/etc/systemd/*; do
    install -v -m 755 -o root -g root ${file} /etc/systemd/system/
done

# reload systemd
systemd daemon-reload

# END-OF-FILE
