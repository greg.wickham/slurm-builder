# The Ibex Slurm Architecture

On CentOS the traditional approach to installing a package is to use an RPM. However this has some significant disadvantages when it comes to certain types of software. The approach that has been taken with Ibex is to have slurm installed onto an NFS share from to which all nodes have access. There are reasons for this:
 - multiple versions of slurm can co-exist
 - the slurm version across the cluster can be consistent
 - non-release versions of slurm can be tested (without packaging)
 - nodes can be arbitrarily swapped between different slurm versions

 # The Ibex Slurm Architecture

## Summary

On CentOS the traditional approach to installing a package is to use an RPM. However this has some significant disadvantages when it comes to certain types of software. The approach that has been taken with Ibex is to have slurm installed onto an NFS share from to which all nodes have access. There are reasons for this:
 - multiple versions of slurm can co-exist
 - the slurm version across the cluster can be consistent
 - non-release versions of slurm can be tested (without packaging)
 - nodes can be arbitrarily swapped between different slurm versions

## How it works

This repo contains a **Makefile** that will download, unpack, build and install a slurm instance.

To mitigate against an installed slurm instance being overwritte, slurm is installed into a specific directory:

    /opt/slurm/install/slurm-<release>-<git hash>-<OS>-<OS Release>

 - `release` = the official slurm release version (ie: **18.08.1**)
 - `git hash` = the HEAD hash of this repo (ie: **dce739ac8aaec42745d8d8d6058b9355e9db41c4**)
 - `OS` = The Operating System designator (ie: **lsb_release --id**)
 - `OS Release` = The release of the build OS (ie: **lsb_release --release**)
 
 The installed version mitigates against accidental overwrites by refusing to run if this git repo has any uncommitted changes pending. Thus a unique `git hash` is ensured. The remainder of the installed version definition permits easy identification of other interesting aspects of the install (ie: slurm version, build OS, etc ).

 To install a slurm version simply take `make` in the top directory:

    $ make
    ...
    $



## Makefile.variables

The builder should never need to change the core `Makefile` to implement slurm updates, instead editing `Makefile.variables`. The variables are:

 - `SLURM_RELEASE` = the release of *slurm* to build (will be downloaded if necessary)
 - `SLURM_SHA1` = the SHA1 checksum of the binary release package.
 - `SLURM_PACKAGES` = RPM packages that are required to be installed before building can proceed. If any package is missing then the `make` will fail.
 - `SLURM_CONFIGURE` = The parameters provided to the `./configure` for slurm.
 - `SLURM_SPANK` = The git repos of any *spank* plugins that also need to be built. With Ibex we use the **private tmp** plugin, but others can be added here. Of note, these should be _local_ copies of public repos.

## systemd

Unit files for **systemd** are installed into `/opt/slurm/install/..../etc`. Before starting slurm these need to be copied to `/usr/lib/systemd/system`.

## profile.d/slurm.sh

A *profile.d* script is installed into `/opt/slurm/install/..../etc`. This needs to be copied to `/etc/profile.d/` so that the slurm user commands are available from the command line.

## slurm configuration

The slurm configration data is stored on a NFS share which permits coherent configurations between all hosts.

To permit the swift changing of hosts between clusters (primarily for testing) the directory structure is:

    /opt/slurm/cluster/<clustername>/slurm/<slurm configuration files>
    /opt/slurm/cluster/<clustername>/nhc/<nhc configuration files>

with symbolic links in /etc being:

    /etc/slurm -> /opt/slurm/cluster/<clustername>/slurm/
    /etc/nhc   -> /opt/slurm/cluster/<clustername>/nhc/

## Booting a node

### puppet

With a node running puppet booting is relatively simple as puppet has already installed the correct files into the necessary locations. All information is in the puppet module **slurm**.

### metabolic

The **slurm** gene of metabolic needs to be seeded but will then setup and start slurm as necessary.

In alleles/slurm/.common/raven.* ensure:

    export SLURM_CLUSTER="raven"
    export SLURM_INSTALL="<install version>"

When the gene runs it will:

 - setup the symbolic link for /etc/slurm to the correct cluster configuration directory
 - install nhc
 - setup the symbolic link for /etc/nhc to the correct cluster configuration directory
 - install the profile.d scripts into /etc/profile.d
 - install the ld.so.conf.d helpder for ../lib
 - copy the systemd unit files from /opt/slurm/install/.../etc/*.service to /usr/lib/systemd/system
 - start the correct daemons using systemd

