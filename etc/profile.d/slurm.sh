# setup PATH so user slurm commands will work.

if [[ -d "/opt/slurm/install/slurm-@SLURM_INSTALL@/bin" ]]; then
    PATH=${PATH}:/opt/slurm/install/slurm-@SLURM_INSTALL@/bin
fi

if [[ -d "/opt/slurm/scripts/bin" ]]; then
    PATH=${PATH}:/opt/slurm/scripts/bin
fi

