# Ibex Slurm Installer

This repo contains a Makefile and associated mechanims to
install a specific version of slurm onto Ibex.

## To Install

Type `make`

The makefile will use a combination of both the slurm version
and git hash from the HEAD of this repo.

The 'make' will abort if there are any differences between
the git repo and the current tree.

The installed version will be located in `/opt/slurm/install/slurm-<INSTALL_HASH>`.

The install hash looks like:

    20.02.3-f4b7f369b1b7c30d9955cdb7241933af2388fddd-CentOS-7.7.1908-MLNX

which is:

    <slurm version>-<git head>-<OS Version>-<OFED type>

The makefile will not let a second build overwrite an earlier
build. If a new build is required then something must have changed
and hence it will use the commit hash from this repo to identify
the new installation.

### Make options

    make -DBUILD_DIR=<build_dir>

BUILD_DIR = Relative path to build directory (default: build)

    make -DINSTALL_BASE=<install base>

INSTALL_BASE = Absolute path to install base (default: /opt/slurm/install)

    make -DINSTALL_DIR=<install dir>

INSTALL_DIR = Absolute path to the final installation directory

    make -DSLURM_RELEASE=<slurm version>

SLURM_RELEASE = Slurm version to build

    make -DPMIX_RELEASE=<pmix version>

PMIX_RELEASE = PMIX version to build

## Warning: Do not overwrite a previous install

Due to Slurms extensive use of shared libraries it is not recommended
to perform an install over the top of an existing install which is
actively being used. Failures can result from the daemons.

## Packages required

RPMs that are required to be installed to successfully build
slurm are defined with `SLURM_PACKAGES` in `Makefile.variables`.

## System boot time

Before slurm can be started a few things need to happen.

1. Setup /etc/sysconfig/slurm

- This file must have SLURM_VERSION defined.
  ie: slurm-18.08.1-b781da2a5a5168eaf760e040357f85bf82dd56c5-CentOS-7.5.1804

2. Run the *setup* script:

