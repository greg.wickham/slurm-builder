#!/usr/bin/env python3

import sys
import re
import os

libraries = {}

for filename in sys.argv:

    with os.popen("/usr/bin/ldd %s" % filename ) as text:
        for line in text:
            # libpthread.so.0 => /lib64/libpthread.so.0 (0x00007fa215cca000)

            #matches = re.search('^([^\s]+)\s+=\>\s+(\/[^\s]+)\s+\(([^\]+)\)', line )
            matches = re.search('^\s*([^\s]+)\s+=\>\s+(\/[^\s]+)', line )

            if matches is not None:
                if matches.group(2) not in libraries:
                    libraries[ matches.group(2) ] = None
                #print "%s -> %s" % ( matches.group(1), matches.group(2) )

packages = {}

for library in libraries:

    with os.popen("/usr/bin/rpm -qif %s" % library ) as text:
        for line in text:
            matches = re.match( '^Name\s*:\s+(.+)$', line )
            if matches is not None:
                packages[ matches.group(1) ] = None

for package in sorted( packages ):
    print( package )

